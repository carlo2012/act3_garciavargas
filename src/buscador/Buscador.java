package buscador;

/**
 * @author Carlo
 * @param <E>
 */

public class Buscador<E extends Comparable<E>> {
    private E[] elementos;
    
    public Buscador(E[] elementos){     
        int E;
    }

    public int hacerBusquedaSecuencial(E elementoBuscado){ 
            int lugar = -1;
            for(int i=0; i < elementos.length; i++){ //Se recorre todo el arreglo para encontrar el elemento deseado
                if(elementos[i] == elementoBuscado){ //Se comparan los elementos del arreglo para verificar si es el que estamos buscando
                    lugar = i; // Aqui es cuando el elemento esta ubicado en el lugar i del arreglo
                break; //Se deja de recorrer el arreglo puesto que ya encontramos el elemento Buscado
                }
            }
            return lugar;
        }
    
        public int hacerBusquedaBinaria(E elementoBuscado){
        int elementoIzquierdo = 0;
        int elementoDerecho = elementos.length -1;
        
        while (elementoIzquierdo <= elementoDerecho){
            int elementoCentral = (elementoIzquierdo + elementoDerecho)/2; //el numero total de elementos se divide a la mitad para encontrar el elemento mas rapido
            E mitad = elementos[elementoCentral]; //El elemento que está en el centro se descarta junto con la mitad del conjunto de elementos
            int s = elementoBuscado.compareTo(mitad); //El elemento que estamos buscando se compara con el elemento central
            if(s < 0){ //si los elementos buscados no corresponden con el que está a la mitad de los arreglos, se vuelven a dividir hasta que el elemento buscado sea encontrado
                elementoDerecho = elementoIzquierdo - 1;
            }else
                if(s > 0){
                    elementoIzquierdo = elementoDerecho + 1;
            }else{ 
                return elementoCentral; //El elemento central es el elemento buscado
            }
        }
        return -1;
    }
        
    public static void main(String[] args){
        String[] E = {"1","2","3","4","5","6"};
     }   
}
